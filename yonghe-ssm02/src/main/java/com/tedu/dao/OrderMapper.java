package com.tedu.dao;

import java.util.List;

import com.tedu.pojo.Door;
import com.tedu.pojo.Order;

public interface OrderMapper {
	
	List<Order> findAll();
	
	void deleteOrderById(Integer orderId);
	void addOrder(Order order);
	
	Order findOrderById(Integer orderId);
	void updateOrderById(Order door);
	
	


}

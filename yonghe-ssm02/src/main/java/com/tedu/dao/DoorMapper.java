package com.tedu.dao;

import java.util.List;

import com.tedu.pojo.Door;

public interface DoorMapper {
	
	List<Door> findAll();
	
	void deleteDoorById(Integer doorId);
	/** 3、新增门店信息 */
	void inserDoor(Door door);
	
	Door findDoorById(Integer doorId);

	void updateDoorById(Door door);
	
	


}

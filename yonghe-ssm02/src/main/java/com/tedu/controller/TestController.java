package com.tedu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tedu.dao.DoorMapper;
import com.tedu.pojo.Door;
/**测试springmvc开发环境 */
@Controller
public class TestController {
	@Autowired
	private DoorMapper doorService;
	
	
	
	/**测试springmvc开发环境 */
	@RequestMapping("/testmvc")
	public String testmvc(){
		return "test";
	}
	
	/** 自动装配：由spring自动为属性赋值(对象)  */
	@Autowired
	DoorMapper mapper;
	
	@RequestMapping("/testSSM")
	public String testSSM(){
		System.out.println("TestSSM.testSSM().............");
		//1.调用findAll方法查询所有门店信息
		List<Door> list = mapper.findAll();
		//2.遍历所有门店信息
		for(Door door : list){
			System.out.println(door);
		}
		return "test";
	}
	
}

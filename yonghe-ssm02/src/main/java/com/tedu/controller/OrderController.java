package com.tedu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tedu.dao.DoorMapper;
import com.tedu.dao.OrderMapper;
import com.tedu.pojo.Door;
import com.tedu.pojo.Order;

@Controller
public class OrderController {

	@Autowired
	private OrderMapper orderMapper;   //获取OrderMapper接口的子类实例

	@Autowired
	private DoorMapper doorMapper;   //获取DoorMapper接口的子类实例

	/**业务需求：查询所有订单信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/orderList
	 */
	@RequestMapping("/orderList")
	public String orderList(Model model) {
		//查询所有订单信息, 将查询到的订单集合存入Model中
		List<Order> oList = orderMapper.findAll();
		model.addAttribute( "oList", oList );
		//查询所有门店信息, 将所有门店信息集合存入Model
		List<Door> dList = doorMapper.findAll();
		model.addAttribute( "dList", dList );
		//跳转到订单列表页面,显示所有订单信息
		return "order_list";
	}
	
	
	/**业务需求：根据订单id删除订单信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/orderDelete?id=2
	 */
	
	@RequestMapping("/orderDelete")
	public String orderDelete(Integer id) {
		orderMapper.deleteOrderById(id);
		return "redirect:/orderList";
	}

	
	
	/**业务需求：新增订单信息
	 * 说明：、修改"新增订单"链接地址，点击链接访问 /findAllDoorToOrderAdd地址，查询所有门店信息，将所有门店信息转发带到order_add.jsp页面中，并在下拉框中进行回显
	 * Request URL: http://localhost:8080/yonghe-ssm02/toOrderAdd
	 * 
	 */
	@RequestMapping("toOrderAdd")
	public String toOrderAdd(Model model) {
		//调用doorMapperd的findAll方法查询所有门店信息，
		List<Door> dList=doorMapper.findAll();
		//将查询到的所有门店信息存到Model中，转到带到order_add页面进行数据的回显
		model.addAttribute("dList",dList);
		//跳转到订单新增页面(order_add.jsp)
		return "order_add";
	}
	
	
	/**新增订单信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/orderAdd
	 */
	@RequestMapping("orderAdd")
	public String orderAdd(Order order) {
		//调用orderMapper的addOrder方法, 新增订单信息
		orderMapper.addOrder(order);
		//添加完后跳转到查询所有订单的方法, 显示最新的订单信息
//		return "redirect:/orderList";//重定向到订单列表页面，回显最新的订单信息
		return "forward:/orderList";
		
	}
	
	/***根据订单id查询订单信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/orderInfo?id=1
	 */
	@RequestMapping("/orderInfo")
	public String orderInfo(Integer id,Model model) {
		//调用orderMapper的findOrderById方法，根据id查询订单信息,
		Order order=orderMapper.findOrderById(id);
		//将查询到的订单信息存入Model中，
		model.addAttribute("order",order);
		//查询所有门店信息, 
		List<Door> dList=doorMapper.findAll();
		//将所有门店信息集合存入Model
		model.addAttribute("dList",dList);
		//转发带到order_update.jsp进行数据的回显
		return "order_update";
	}
	
	/**根据订单id更新订单信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/orderUpdate
	 */
	@RequestMapping("/orderUpdate")
	public String orderUpdate(Order order) {
		orderMapper.updateOrderById(order);
		return "forward:/orderList";
		
		
	}
	
}

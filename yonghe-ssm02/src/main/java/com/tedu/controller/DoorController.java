package com.tedu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tedu.dao.DoorMapper;
import com.tedu.pojo.Door;

@Controller
public class DoorController {
	
	/* 获取DoorMapper接口的子类实例 
	 * 由于前面我们在applicationContext.xml中配置了扫描dao包下的所有接口,由
	 * 	spring框架为接口的创建子类实例,并将接口的子类实例存到spring的map中
	 * @Autowired作用是,根据类型(DoorMapper)到spring的map中找这个接口对应
	 * 	的子类实例(即实现类),找到之后会将接口的子类实例赋值给当前这个成员变量. */
	
	@Autowired
	private DoorMapper doorService;
	
	/** 1、查询所有的门店信息 */
	@RequestMapping("/doorList")
	public String doorList(Model model) {
		//调用service层的findAll方法，查询所有的门店信息
		List<Door> list = doorService.findAll();
		//将门店集合存入到Model中
		model.addAttribute("list",list);
//		for (Door d:list) {
//			System.out.println(d);
//		}
		//跳转到门店列表(door_list.jsp)页面(在该页面中展示所有门店信息)
		return "door_list";
	}
	
	
	/**根据id删除门店信息
	 * 
	 * Request URL: http://localhost:8080/yonghe-ssm02/doorDelete?id=1
	  */
	@RequestMapping("/doorDelete")
	public String doorDelete(Integer id) {
		//调用DoorMapper的deleteById方法, 根据id删除门店信息
		doorService.deleteDoorById(id);
		//删除成功后,跳转到查询门店的方法, 显示最新门店信息
		return "forward:/doorList";
	}
	
	
	/**新增门店信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/doorAdd
	 * 
	 * 接收客户端提交过来的门店信息(name,tel,addr)
	*/
	@RequestMapping("/doorAdd")
	public String doorAdd(Door door) {
		doorService.inserDoor(door);
		//跳转到查询所有门店的方法, 显示最新门店信息
		return "forward:/doorList";
	}
	
	
	

	/**、根据id查询门店信息
	 * 将查询到的门店信息带到门店修改页面, 进行数据的回显
	 * 
	 * Request URL: http://localhost:8080/yonghe-ssm02/doorInfo?id=29
	 * */
	@RequestMapping("/doorInfo")
	public String doorInfo(Integer id,Model model) {
		//调用DoorMapper的findById方法, 根据id查询门店信息
		Door door=doorService.findDoorById(id);
		//根据id查询到的门店信息存到model中，从而将门店信息带回门店修改页面, 进行数据的回显
		model.addAttribute("door",door);
		//转发到门店修改页面
		return "door_update";
	}

	
	/**根据id更新门店信息，更新完后，显示最新的门店信息
	 * Request URL: http://localhost:8080/yonghe-ssm02/doorUpdate
	 */
	@RequestMapping("/doorUpdate")
	public String doorUpdate(Door door) {
		doorService.updateDoorById(door);
		return "redirect:/doorList";
	}
}

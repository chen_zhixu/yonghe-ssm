package com.tedu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/* @Controller (1)标识当前类属于Controller层
 * 	(2)spring容器会扫描当前包下的所有类,如果类上有@Controller注解
 * 	将会由spring容器创建该类的实例
 */
@Controller

public class PageController {
	
	
	
	/* 	声明一个通用的页面跳转方法: 通过访问jsp的名字
	 * 	可以跳转到/WEB-INF/pages/下面指定名字的jsp页面
	 * 	其中{}中的 jspName 用于接收访问的路径名, 例如: 
	 * 		访问地址为: localhost/项目名称/index
	 * 		此时, jspName的值就是 index
	 * 	再将{}中jspName的值传递给方法上的形参jspName,值也为index
	 * 	最后再将jspName作为返回值返回, 也就是跳转到指定名称的jsp
	 * 
	 */
	
	/* 提供通用的页面跳转方法 
	 * 	/{}中的jspName用于获取(接收)访问的路径名
	 * 	例如访问路径为:../_top,那么jspName的值就是 "_top"
	 * 	@PathVariable注解,用于将  /{}中jspName的值 作为实参传递给
	 * 	方法上的形参 jspName, 此时形参jspName的值也为 "_top", 最终将
	 * 	"_top"直接return。即跳转到 _top.jsp
	 * 	总结: 当访问的路径名为 xxx, 就可以跳转到 xxx.jsp
	 */
	@RequestMapping("/{jspName}")
	public String toJsp(@PathVariable String jspName) {
		System.out.println("-----执行通用的页面跳转方法,跳转到 ["+jspName+".jsp]...");
		return jspName; //可以跳转到任何的jsp
		
	}
	
	
	

	
}
